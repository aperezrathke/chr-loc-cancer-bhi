#!/usr/bin/python
# -*- coding: utf-8 -*-

import os

# Path to directory containing this script
# https://stackoverflow.com/questions/4934806/how-can-i-find-scripts-directory-with-python
SCRIPT_DIR = os.path.abspath(os.path.dirname(os.path.realpath(__file__)))

# Path to INI
CONF_PATH = os.path.join(SCRIPT_DIR, "config", "cb.K562.ini")
CONF_ARG = "--conf " + CONF_PATH

# Output base directory
OUT_DIR = os.path.join(SCRIPT_DIR, "..", "output")

# Target data directory
DATA_DIR = os.path.join(OUT_DIR, "null.K562")
OUT_ARG = "--output_dir " + DATA_DIR

# Exe path
EXE_PATH = os.path.join(OUT_DIR, "build", "bin", "Release_threaded", "u_sac")

# Determine dispatch
DISPATCH = "cancer-bhi"
DISPATCH_ARG = "--null_dispatch " + DISPATCH

# Job identifier prefix
JOB_PREFIX = "cb"

# Export options
EXPORT_ARG = "-export_csv -export_pdb -export_log_weights -export_ligc_csv_gz"

# Initialize variables
current = 0
target = 10000
step = 100
job_id = 0

while current < target:
    ens_size = min (step , target - current)
    job_name = JOB_PREFIX + "." + str(job_id)
    # Derive arguments
    ens_size_arg = "--ensemble_size " + str(ens_size)
    job_id_arg = "--job_id " + job_name
    # Build command line
    str_cmd = EXE_PATH + " " + DISPATCH_ARG + " "
    str_cmd += ens_size_arg + " " + job_id_arg + " "
    str_cmd += CONF_ARG + " " + EXPORT_ARG + " " + OUT_ARG + " " 
    # Run command line
    print "Running: " + str_cmd
    result = os.popen(str_cmd).read()
    print result
    current+=ens_size
    job_id += 1

print "Finished."
