# Path to this script
###################################################################

# Cached directory to script
# https://stackoverflow.com/questions/3452086/getting-path-of-an-r-script
SHELL_SCRIPT_DIR = getSrcDirectory(function(x) {
  x
})

# getSrcDirectory trick will not work via Rscript. If this is the
# case, then SHELL_SCRIPT_DIR will be length zero.
# http://stackoverflow.com/questions/1815606/rscript-determine-path-of-the-executing-script
if (length(SHELL_SCRIPT_DIR) == 0) {
  SHELL_SCRIPT_DIR = dirname(scriptName::current_filename())
}

# @return Path to directory containing this script
get_shell_script_dir <- function() {
  return(SHELL_SCRIPT_DIR)
}
###################################################################
# Defaults
###################################################################
# Default cell identifier
DEF_CID = "gm12878"

###################################################################
# Source utility script(s)
###################################################################
source(file.path(get_shell_script_dir(), "hic_clean.R"))

# Null cache utilities
if (!exists("SOURCED_null_cache")) {
  source(file.path(get_shell_script_dir(), "null_cache.R"))
}

source(file.path(get_shell_script_dir(), "heat_core.R"))
###################################################################
# Defaults
###################################################################
# Default chromosome identifier
DEF_CHR_ID = "chr22"
#get_freq_mat <-
#function() {
   hic_clean_df = load_rdata(get_hic_clean_ecdf_path("K562"), verbose = TRUE)
   hic_dense = matrix(data = 0.0,
                   nrow = 400,
                   ncol = 400)
 
   hic_coo = hic_clean_df[(hic_clean_df$i1 <= 400) & (hic_clean_df$j1 <= 400) & (hic_clean_df$fdr <= 0.05),]
   hic_ij = as.matrix(hic_coo[, c("i1", "j1")])
   hic_dense[hic_ij] = hic_coo$fq

   d = diag(hic_dense)
   hic_dense[lower.tri(hic_dense, diag = T)] = 0
   hic_dense = hic_dense + t(hic_dense)
   diag(hic_dense) <- d
 #  return (hic_dense)
#}

hic_heat_df = heat_ligc (mat= hic_dense,             
                         vmin = DEF_HEAT_VMIN,
                         vmax = DEF_HEAT_VMAX,
                         as_rank = TRUE,
                         div_by_max = TRUE,
                         col_ = DEF_HEAT_COL,
                         tick_by = DEF_HEAT_TICK_BY,
                         main = NULL,
                         sub = NULL,
                         xlab = NULL,
                         ylab = NULL,
                         outer = FALSE,
                         ptsov_utri_x = NULL,
                         ptsov_utri_y = NULL,
                         ptsov_mirror = TRUE,
                         ptsov_1base = TRUE,
                         ptsov_col = "black",
                         ptsov_pch = 0,
                         ptsov_cex = 1,
                         ptsov_lwd = 1)
#dev.copy2pdf(file = "my.pdf",width=5,height = 3)
# Launcher for heatmap for Hi-C
#pdf(file = if(onefile) "Rplots.pdf" else "Rplot%03d.pdf",
 #   width, height, onefile, family, title, fonts, version,
  #  paper, encoding, bg, fg, pointsize, pagecentre, colormodel,
   # useDingbats, useKerning, fillOddEven, compress)