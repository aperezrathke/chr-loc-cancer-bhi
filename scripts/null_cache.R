###################################################################
# null_cache.R
#
# Utilities for converting to binary RData format
###################################################################

# Guard variable to avoid multiple sourcing
SOURCED_null_cache = TRUE

###################################################################
# Path to this script
###################################################################

# Cached directory to script
# https://stackoverflow.com/questions/3452086/getting-path-of-an-r-script
NULL_CACHE_SCRIPT_DIR = getSrcDirectory(function(x) {
  x
})

# getSrcDirectory trick will not work via Rscript. If this is the
# case, then NULL_CACHE_SCRIPT_DIR will be length zero.
# http://stackoverflow.com/questions/1815606/rscript-determine-path-of-the-executing-script
if (length(NULL_CACHE_SCRIPT_DIR) == 0) {
  NULL_CACHE_SCRIPT_DIR = dirname(scriptName::current_filename())
}

# @return Path to directory containing this script
get_null_cache_script_dir <- function() {
  return(NULL_CACHE_SCRIPT_DIR)
}

###################################################################
# Source utility script(s)
###################################################################

# Import/export utilities
source(file.path(get_null_cache_script_dir(), "utils.io.R"))

###################################################################
# Defaults
###################################################################

# Default: If TRUE, overwrite cached data
DEF_SHOULD_OVERWRITE = FALSE

# Default: If TRUE, verbose logging to stdout
DEF_VERBOSE = TRUE

# If True, samples with weights outside +/- 1.5 IQR are removed
DEF_REMOVE_OUTLIERS = TRUE

# Default cell identifier
DEF_CID = "gm12878"

# Default, if TRUE, bonded ligations are retained
DEF_NULL_BONDED = FALSE

###################################################################
# Additional paths
###################################################################

# Output base directory
OUT_BASE_DIR = file.path(get_null_cache_script_dir(), "..", "output")

# @return Output null data folder for cell identifier
get_out_null_dir <- function(cid = DEF_CID) {
  subd = paste0("null.", cid)
  return(file.path(OUT_BASE_DIR, subd))
}

# @return Path to folder with null ligc in rdata format
get_null_ligc_rdata_dir <- function(cid = DEF_CID) {
  return(file.path(get_out_null_dir(cid = cid), "ligc.rdata"))
}

# @return Path to folder with null ligc in csv gzipped format
get_null_ligc_csv_gz_dir <- function(cid = DEF_CID) {
  return(file.path(get_out_null_dir(cid = cid), "ligc.csv.gz"))
}

# @return Path to Rdata merged ligation contact folder
get_null_ligc_merge_rdata_dir <- function(cid = DEF_CID) {
  return(file.path(get_out_null_dir(cid = cid), "ligc.merge.rdata"))
}

# @return Path to RData with merged ligation contacts
get_null_ligc_merge_rdata_path <- function(cid = DEF_CID) {
  return(file.path(
    get_null_ligc_merge_rdata_dir(cid = cid),
    "ligc.merge.rdata"
  ))
}

# @return Path to CSV GZ symmetric numeric frequency matrix
get_null_ligc_freqmat_csv_gz_path <- function(cid = DEF_CID) {
  fid = "ligc.fregmat.csv.gz"
  return(file.path(get_out_null_dir(cid = cid),
                   fid,
                   fid))
}

# @return Path to RData symmetric numeric frequency matrix
get_null_ligc_freqmat_rdata_path <- function(cid = DEF_CID) {
  fid = "ligc.freqmat.rdata"
  return(file.path(get_out_null_dir(cid = cid),
                   fid,
                   fid))
}

# @return Path to ligc popd BLB replicates folder in gzipped CSV format
get_null_ligc_popd_lband_csv_gz_dir <- function(cid = DEF_CID) {
  return(file.path(get_out_null_dir(cid = cid), "ligc.popd.lband.csv.gz"))
}

# @return Path to ligc popd BLB replicates folder in RData format
get_null_ligc_popd_lband_rdata_dir <- function(cid = DEF_CID) {
  return(file.path(get_out_null_dir(cid = cid), "ligc.popd.lband.rdata"))
}

# @return Path to RData with merged ligc popd BLB replicates folder
get_null_ligc_popd_lband_merge_rdata_path <-
  function(cid = DEF_CID) {
    fid = "ligc.popd.lband.merge.rdata"
    return(file.path(get_out_null_dir(cid = cid), fid, fid))
  }

###################################################################
# Cache
###################################################################

# @param logw - log-transformed importance weights
# @return Effective sample size
get_ess <- function(logw) {
  w = exp(logw - max(logw))
  v = var(w)
  m = mean(w)
  ess = 1.0 / (1.0 + (v / (m * m)))
  return(ess)
}

# Utility converts CSV formatted ligation contacts into binary
# RData. Specifically, each file is converted into a list with two
# members:
#   $logw : log-transforwed weight as numeric scalar
#   $ligc : |NUM_CONTACTS| x 2 integer index (1-based) matrix where
#       each row is an (i,j) tuple indicating that the monomers at
#       1-based indices i and j were within the ligation threshold.
#       Note that NUM_CONTACTS is specific to each sample.
# @WARNING: Converts to 1-based indexing!
# @param in_csv_dir - Input directory containing ligation contact
#   data in [gzipped] CSV format
# @param out_rdata_dir - Output directory to store binary Rdata
# @param CONFUNC - Open connection callback, uses first argument
# @param verbose - If TRUE, log to stdout
null_cache_ligc_csv <-
  function(in_csv_dir = get_null_ligc_csv_gz_dir(),
           out_rdata_dir = get_null_ligc_rdata_dir(),
           CONFUNC = c(gzfile, file),
           verbose = DEF_VERBOSE) {
    CONFUNC = CONFUNC[[1]]
    # Log weight prefix
    comment = "#"
    log_weight_prefix = paste0(comment, " LOG WEIGHT: ")
    log_weight_prefix_len = nchar(log_weight_prefix) + 1
    # Get list of input ligc files
    fnames_in = list.files(
      path = in_csv_dir,
      all.files = FALSE,
      full.names = FALSE,
      recursive = FALSE,
      include.dirs = FALSE,
      no.. = TRUE
    )
    # Iterate over ligc files
    for (fname_in in fnames_in) {
      fpath_in = file.path(in_csv_dir, fname_in)
      if (verbose) {
        print(paste0("Reading file: ", fpath_in))
      }
      # Open [compressed] file handle
      con = CONFUNC(description = fpath_in)
      # Find line with log weight
      is_logw_line = FALSE
      repeat {
        l = readLines(con, n = 1)
        # Test line was read and is a comment
        if ((length(l) == 1) && startsWith(l, comment)) {
          # Test line is a log weight comment
          if (startsWith(l, log_weight_prefix)) {
            # Success
            is_logw_line = TRUE
            break
          }
        } else {
          # Fail
          break
        }
      }
      if (!is_logw_line) {
        # We did not find a log weight!
        print(paste0("Warning: unable to read log weight (skipping): ",
                     fin_path))
        close(con)
        next
      }
      # Parse log weight
      log_weight = as.numeric(substr(l, log_weight_prefix_len,
                                     nchar(l)))
      # Parse ligation contacts
      # Note: This closes the file connection!
      ligc = as.matrix(read.csv(
        file = con,
        header = FALSE,
        stringsAsFactors = FALSE,
        comment.char = "#"
      ))
      # Convert to 1-based indexing
      ligc = ligc + 1
      # Make sure mode is integer
      mode(ligc) = "integer"
      # Combine log weight and contacts into single tuple
      tuple = list("logw" = log_weight, "ligc" = ligc)
      # Save file
      fname_out = file_path_sans_sub_ext(x = fname_in,
                                         sub_ext_no_dot = "csv")
      fname_out = smush(fname_out, "rdata")
      fpath_out = file.path(out_rdata_dir, fname_out)
      save_rdata(
        data = tuple,
        rdata_path = fpath_out,
        verbose = verbose,
        create_subdirs = TRUE
      )
    }
  }

# Combines all individual ligc RData files into single RData file.
# Specifically, creates a single list structure with members:
#   $logw - a parallel numeric vector of log-transformed weights
#   $ligc - a parallel list of |NUM_CONTACTS| x 2 integer index
#     (1-based) matrices where each row is (i,j) tuple indicating
#     that monomers at 1-based indices i and j where within the
#     ligation threshold distance. Note that NUM_CONTACTS is
#     specific to each sample.
# @WARNING: Assumes null_cache_ligc_csv() has already been called!
# @param rmvout - If TRUE, remove outlier samples with weights not
#   in inclusive range: [Q1 - 1.5 IQR, Q3 + 1.5 IQR]
# @param in_rdata_dir - Input RData directory with individual
#   ligation contact tuples
# @param out_rdata_path - Output path for merged ligation contact
#   data
# @param should_overwrite - Overwrite any existing cached data
# @param verbose - If TRUE, log to stdout
# @return list of merged ligation contact tuples
null_cache_ligc_merge <-
  function(rmvout = DEF_REMOVE_OUTLIERS,
           in_rdata_dir = get_null_ligc_rdata_dir(),
           out_rdata_path = get_null_ligc_merge_rdata_path(),
           should_overwrite = DEF_SHOULD_OVERWRITE,
           verbose = DEF_VERBOSE) {
    # Check if cached file exists
    if (is.character(out_rdata_path) &&
        file.exists(out_rdata_path) &&
        !should_overwrite) {
      if (verbose) {
        print("Loading cached data")
      }
      return(invisible(load_rdata(
        rdata_path = out_rdata_path, verbose = verbose
      )))
    }
    # Else, process individual RData tuples
    # Get list of input ligc files
    fnames_in = list.files(
      path = in_rdata_dir,
      all.files = FALSE,
      full.names = FALSE,
      recursive = FALSE,
      include.dirs = FALSE,
      no.. = TRUE
    )
    # Iterate over ligc files
    logw = rep(0, length(fnames_in))
    ligc = list()
    index = 1
    for (fname_in in fnames_in) {
      fpath_in = file.path(in_rdata_dir, fname_in)
      tuple = load_rdata(rdata_path = fpath_in, verbose = verbose)
      ligc[[index]] = tuple$ligc
      logw[index] = tuple$logw
      index = index + 1
    }
    if (verbose) {
      ess = get_ess(logw)
      print(paste0("ESS (all): ", ess))
    }
    # Remove outliers
    if (rmvout) {
      logw_max = max(logw)
      w = exp(logw - max(logw))
      q = quantile(x = w, probs = c(0.25, 0.75))
      iqr = q["75%"] - q["25%"]
      h = 1.5 * iqr
      w_min = q["25%"] - h
      w_max = q["75%"] + h
      keep = (w >= w_min) & (w <= w_max)
      logw = logw[keep]
      ligc = ligc[keep]
      if (verbose) {
        n_all = length(keep)
        n_rmv = n_all - length(which(keep))
        perc_rmv = 100.0 * n_rmv / n_all
        print(
          paste0(
            "Outlier detection removed ",
            n_rmv,
            " of ",
            n_all,
            " (",
            format(round(perc_rmv, 1), nsmall = 1),
            "%) ligc data samples."
          )
        )
        ess = get_ess(logw)
        print(paste0("ESS (outliers removed): ", ess))
      }
    }
    out = list(logw = logw, ligc = ligc)
    # Cache data
    if (is.character(out_rdata_path)) {
      save_rdata(data = out,
                 rdata_path = out_rdata_path,
                 verbose = verbose)
    }
    return(invisible(out))
  }

# Convert ligc samples into a symmetric contact frequency matrix
# @WARNING: Assumes null_cache_ligc_merge() has already been called!
# @WARNING: Assumes number of bins can be obtained from max
#   encountered (i, j) ligation contact index tuple
# @WARNING: Assumes (i, j) ligation contact tuples are either all
#   lower or all upper triangle (all samples from same triangle!)
# @WARNING: Will set 0-diag and 1-diag equal to 1.0
# @param in_rdata_path - Path to merged ligc data, assumes data is a
#   list with elements:
#     $logw - numeric vector of log weights of length K, where K is
#       total number of [possibly outlier removed] polymer samples
#     $ligc - parallel list of K integer matrices, each matrix has
#       dimensions [NUM_CONTACTS(k) x 2] where NUM_CONTACTS(k) is
#       the number of ligation contacts in the k-th sample, and each
#       row is an (i,j) tuple of 1-based monomer indices that where
#       within the ligation distance threshold
# @param out_csv_gz_path - Path to write contact frequency matrix in
#   gzipped CSV format
# @param out_rdata_path - Path to write contact frequency matrix in
#   binary RData format
# @param should_overwrite - If TRUE, overwrite any cached data
# @param verbose - If TRUE, log to stdout
# @return Symmetric numeric frequency matrix, each cell in [0,1]
null_cache_ligc_freqmat <-
  function(in_rdata_path = get_null_ligc_merge_rdata_path(),
           out_csv_gz_path = get_null_ligc_freqmat_csv_gz_path(),
           out_rdata_path = get_null_ligc_freqmat_rdata_path(),
           should_overwrite = DEF_SHOULD_OVERWRITE,
           verbose = DEF_VERBOSE) {
    # Check if cached file exists
    if (is.character(out_rdata_path) &&
        file.exists(out_rdata_path) &&
        !should_overwrite) {
      if (verbose) {
        print("Loading cached data")
      }
      return(invisible(load_rdata(
        rdata_path = out_rdata_path, verbose = verbose
      )))
    }
    # Else, load merged ligation contact samples
    if (verbose) {
      print("Loading merged ligation contacts")
    }
    ligc_lst = load_rdata(rdata_path = in_rdata_path, verbose = verbose)
    stopifnot(length(ligc_lst$logw) == length(ligc_lst$ligc))
    K = length(ligc_lst$ligc)
    stopifnot(K > 0)
    # Find number of bins as max observed 1-based ligation index
    max_ixs = sapply(X = ligc_lst$ligc,
                     FUN = max,
                     simplify = TRUE)
    num_bin = max(max_ixs)
    if (verbose) {
      print(paste0("Found ", num_bin, " contact bins"))
    }
    stopifnot(num_bin > 1)
    # Allocate matrix
    out = matrix(data = 0.0,
                 nrow = num_bin,
                 ncol = num_bin)
    # Normalize weights
    w = exp(ligc_lst$logw - max(ligc_lst$logw))
    w = w / sum(w)
    # Process samples
    if (verbose) {
      print(paste0("Processing ", K, " samples..."))
    }
    for (i in 1:K) {
      # Weighted update
      out[ligc_lst$ligc[[i]]] = out[ligc_lst$ligc[[i]]] + w[i]
    }
    # Create symmetric matrix
    diag(out) = 0.0
    # Help verify all samples either a) all lower tri or b) all
    # upper tri by checking that one of the tris is zero
    stopifnot(all(out[lower.tri(out)] == 0.0) ||
                all(out[upper.tri(out)] == 0.0))
    # Reflect non-zero triangle to the other triangle
    out = out + t(out)
    # Update 0-diagonal
    diag(out) = 1.0
    # Update |1|-diagonals
    # https://stackoverflow.com/questions/30631531/how-does-one-get-the-kth-diagonal-in-r-what-about-the-opposite-diagonals
    delta = col(out) - row(out)
    out[abs(delta) == 1] = 1.0
    stopifnot(isSymmetric(out))
    # Save (compressed) plain-text
    if (is.character(out_csv_gz_path)) {
      save_csv_gz(x = out,
                  gz_path = out_csv_gz_path,
                  verbose = verbose)
    }
    # Save binary
    if (is.character(out_rdata_path)) {
      save_rdata(data = out,
                 rdata_path = out_rdata_path,
                 verbose = verbose)
    }
    return(invisible(out))
  }

# Convert genomic distance to 1-based integer index for diagonal
# @param gd - genomic distance (non-negative)
# @return 1-based diagonal index key for genomic distance
gd2ixdiag <- function(gd) {
  return(gd + 1)
}

# Convert 1-based diagonal integer index to genomic distance
# @param ix - 1-based diagonal index key
# @return Corresponding genomic distance
ixdiag2gd <- function(ix) {
  return(ix - 1)
}

# Utility converts lower-banded, CSV formatted ligation contact
# bootstrap replicates into binary RData. Specifically, converts
# each sample into a list keyed by diagonal's (genomic distance+1).
# For instance, if the sample has 5 diagonals, 3 of which are non-
# bonded, and bonded is FALSE, then the resulting list will look
# like the following:
#   [[1]] = NULL
#   [[2]] = NULL
#   [[3]] = numeric vector of length N - 2
#   [[4]] = numeric vector of length N - 3
#   [[5]] = numeric vector of length N - 4
# where 'N' is the number of monomer positions (in this case 5). See
# gd2ixdiag(gd) for converting genomic distance 'gd' to list key and
# ixdiag2df(ix) for converting list key back to genomic distance.
# @param in_csv_dir - Input directory containing lower banded
#   bootstrap replicates for ligation contact data in [gzipped] CSV
#   format
# @param out_rdata_dir = Output directory to store binary Rdata
# @param CONFUNC - Open connection callback, uses first argument
# @param bonded - If TRUE, bonded ligations are assumed present
# @param verbose - If TRUE, log to stdout
null_cache_ligc_popd_lband_csv <-
  function(in_csv_dir = get_null_ligc_popd_lband_csv_gz_dir(),
           out_rdata_dir = get_null_ligc_popd_lband_rdata_dir(),
           CONFUNC = c(gzfile, file),
           bonded = DEF_NULL_BONDED,
           verbose = DEF_VERBOSE) {
    CONFUNC = CONFUNC[[1]]
    # Start genomic distance diagonal
    GD_START = 2
    if (bonded) {
      GD_START = 0
    }
    # Get list of input files
    fnames_in = list.files(
      path = in_csv_dir,
      all.files = FALSE,
      full.names = FALSE,
      recursive = FALSE,
      include.dirs = FALSE,
      no.. = TRUE
    )
    # Iterate over ligc popd replicates
    for (fname_in in fnames_in) {
      fpath_in = file.path(in_csv_dir, fname_in)
      if (verbose) {
        print(paste0("Reading file: ", fpath_in))
      }
      # Open [compressed] file handle
      con = CONFUNC(description = fpath_in)
      csv_lines = readLines(con)
      close(con)
      out = vector(mode = "list", length = length(csv_lines) + GD_START)
      gd = GD_START
      # Process each lower diagonal
      for (csv_line in csv_lines) {
        # Split diagonal by ',' and convert to numeric
        out[[gd2ixdiag(gd)]] = as.numeric(unlist(strsplit(
          x = csv_line,
          split = ',',
          fixed = TRUE
        )))
        gd = gd + 1
      }
      # Save file
      fname_out = file_path_sans_sub_ext(x = fname_in,
                                         sub_ext_no_dot = "csv")
      fname_out = smush(fname_out, "rdata")
      fpath_out = file.path(out_rdata_dir, fname_out)
      save_rdata(
        data = out,
        rdata_path = fpath_out,
        verbose = verbose,
        create_subdirs = TRUE
      )
    }
  }

# Combines all individual ligc bootsrap replicated RData files into
# single RData file. Specifically, creates a single list structure
# with numeric matrices for each genomic distance diagonal, where
# each row is from a different replicate. For instance, if there
# are 10 bootstrap replicates, 5 total diagonals, bonded=FALSE (see
# null_cache_ligc_popd_lband_csv()), then the resulting list will
# look like the following:
#   [[1]] = NULL
#   [[2]] = NULL
#   [[3]] = 10 x |N - 2| numeric matrix
#   [[4]] = 10 x |N - 3| numeric matrix
#   [[5]] = 10 x |N - 4| numeric matrix
# where 'N' is the number of monomer positions (in this case 5). See
# gd2ixdiag(gd) for converting genomic distance 'gd' to list key and
# ixdiag2df(ix) for converting list key back to genomic distance.
# @WARNING: assumes null_cache_ligc_popd_lband_csv() has already
#   been called!
# @param in_rdata_dir - Input RData directory with individual
#   ligation contact bootstrap replicates
# @param out_rdata_path -Output path for merged ligation contact
#   bootstrap replicate data
# @param should_overwrite - Overwrite any existing cached data
# @param verbose - If TRUE, log to stdout
# @return list of merged ligation contact BLB replicate matrices
#   keyed by (genomic distance + 1) - see gd2ixdiag
null_cache_ligc_popd_lband_merge <-
  function(in_rdata_dir = get_null_ligc_popd_lband_rdata_dir(),
           out_rdata_path = get_null_ligc_popd_lband_merge_rdata_path(),
           should_overwrite = DEF_SHOULD_OVERWRITE,
           verbose = DEF_VERBOSE) {
    # Check if cached file exists
    if (is.character(out_rdata_path) &&
        file.exists(out_rdata_path) &&
        !should_overwrite) {
      if (verbose) {
        print("Loading cached data")
      }
      return(invisible(load_rdata(
        rdata_path = out_rdata_path, verbose = verbose
      )))
    }
    # Get list of input files
    fnames_in = list.files(
      path = in_rdata_dir,
      all.files = FALSE,
      full.names = FALSE,
      recursive = FALSE,
      include.dirs = FALSE,
      no.. = TRUE
    )
    N_REPS = length(fnames_in)
    if (N_REPS < 1) {
      if (verbose) {
        print("Warning: no data found")
      }
      return(NULL)
    }
    # Process first replicate
    func_prealloc <- function(lband, N_REPS) {
      if (is.null(lband)) {
        return(NULL)
      }
      m = matrix(data = 0,
                 nrow = N_REPS,
                 ncol = length(lband))
      m[1,] = lband
      return(m)
    }
    out = list()
    fname_in = fnames_in[1]
    fpath_in = file.path(in_rdata_dir, fname_in)
    lbands = load_rdata(rdata_path = fpath_in, verbose = verbose)
    out = lapply(X = lbands, FUN = func_prealloc, N_REPS)
    # Iterate over remaining replicates
    ix_rep = 2
    fnames_in = fnames_in[-1]
    for (fname_in in fnames_in) {
      fpath_in = file.path(in_rdata_dir, fname_in)
      lbands = load_rdata(rdata_path = fpath_in, verbose = verbose)
      for (ix_band in 1:length(lbands)) {
        lband = lbands[[ix_band]]
        if (!is.null(lband)) {
          out[[ix_band]][ix_rep, ] = lband
        }
      }
      ix_rep = ix_rep + 1
    }
    # Save merged data
    if (is.character(out_rdata_path)) {
      save_rdata(data = out,
                 rdata_path = out_rdata_path,
                 verbose = verbose)
    }
    return(invisible(out))
  }

###################################################################
# Launchers
###################################################################

# Caches ligc data from CSV to RData for each cell identifier
launch_null_cache_ligc_csv <- function(cids = c("gm12878", "K562"),
                                       verbose = DEF_VERBOSE) {
  for (cid in cids) {
    csv_gz_dir = get_null_ligc_csv_gz_dir(cid = cid)
    rdata_dir = get_null_ligc_rdata_dir(cid = cid)
    null_cache_ligc_csv(
      in_csv_dir = csv_gz_dir,
      out_rdata_dir = rdata_dir,
      verbose = verbose
    )
  }
}

# Caches merged ligc data
launch_null_cache_ligc_merge <-
  function(cids = c("gm12878", "K562"),
           rmvout = DEF_REMOVE_OUTLIERS,
           verbose = DEF_VERBOSE) {
    for (cid in cids) {
      in_rdata_dir = get_null_ligc_rdata_dir(cid = cid)
      out_rdata_path = get_null_ligc_merge_rdata_path(cid = cid)
      null_cache_ligc_merge(
        rmvout = rmvout,
        in_rdata_dir = in_rdata_dir,
        out_rdata_path = out_rdata_path
      )
    }
  }

# Caches freqency matrix of ligc samples
launch_null_cache_ligc_freqmat <-
  function(cids = c("gm12878", "K562"),
           verbose = DEF_VERBOSE) {
    for (cid in cids) {
      in_rdata_path = get_null_ligc_merge_rdata_path(cid = cid)
      out_csv_gz_path = get_null_ligc_freqmat_csv_gz_path(cid = cid)
      out_rdata_path = get_null_ligc_freqmat_rdata_path(cid = cid)
      null_cache_ligc_freqmat(
        in_rdata_path = in_rdata_path,
        out_csv_gz_path = out_csv_gz_path,
        out_rdata_path = out_rdata_path
      )
    }
  }

# Caches ligc popd BLB replicates as RData
launch_null_cache_ligc_popd_lband_csv <-
  function(cids = c("gm12878", "K562"),
           verbose = DEF_VERBOSE) {
    for (cid in cids) {
      in_csv_dir = get_null_ligc_popd_lband_csv_gz_dir(cid = cid)
      out_rdata_dir = get_null_ligc_popd_lband_rdata_dir(cid = cid)
      null_cache_ligc_popd_lband_csv(in_csv_dir = in_csv_dir,
                                     out_rdata_dir = out_rdata_dir)
    }
  }

# Caches merged ligc popd BLB replicates
launch_cache_ligc_popd_lband_merge <-
  function(cids = c("gm12878", "K562"),
           verbose = DEF_VERBOSE) {
    for (cid in cids) {
      in_rdata_dir = get_null_ligc_popd_lband_rdata_dir(cid = cid)
      out_rdata_path = get_null_ligc_popd_lband_merge_rdata_path(cid = cid)
      null_cache_ligc_popd_lband_merge(in_rdata_dir = in_rdata_dir,
                                       out_rdata_path = out_rdata_path)
    }
  }
