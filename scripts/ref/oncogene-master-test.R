###################################################################
# Compute "transition" matrices for healthy and cancer phenotypes
###################################################################

library(plyr)

# Guard variable to avoid multiple sourcing
SOURCED_cosmic_trans = TRUE

###################################################################
# Path to this script
###################################################################

# Cached directory to script
# https://stackoverflow.com/questions/3452086/getting-path-of-an-r-script
COSMIC_TRANS_SCRIPT_DIR = getSrcDirectory(function(x) {
  x
})

# getSrcDirectory trick will not work via Rscript. If this is the
# case, then COSMIC_TRANS_SCRIPT_DIR will be length zero.
# http://stackoverflow.com/questions/1815606/rscript-determine-path-of-the-executing-script
if (length(COSMIC_TRANS_SCRIPT_DIR) == 0) {
  COSMIC_TRANS_SCRIPT_DIR = dirname(scriptName::current_filename())
}

# @return Path to directory containing this script
get_cosmic_trans_script_dir <- function() {
  return(COSMIC_TRANS_SCRIPT_DIR)
}

###################################################################
# Source utility script(s)
###################################################################

# Hi-C clean utilities
if (!exists("SOURCED_hic_clean")) {
  source(file.path(get_cosmic_trans_script_dir(), "hic_clean.R"))
}

###################################################################
# Path utilities
###################################################################

# @return Directory path containing AB compartments
get_AB_dir <- function() {
  return(file.path(get_cosmic_trans_script_dir(), "..", "output", "AB"))
}

# Default alternative format cell identifier
DEF_ALTCID = c("GM", "K")[1]

# Default alternative format chromosome
DEF_ALTCHR_ID = "2"

# Convert from alternative chromosome to standard
alt2stdchr <-
  function(altchr_id) {
    return(paste0("chr", altchr_id))
  }

# @return File path to AB data
get_AB_path <-
  function(altcid = DEF_ALTCID,
           altchr_id = DEF_ALTCHR_ID,
           ext_no_dot = "csv") {
    fname = paste0(altcid, "_", altchr_id, "_", "AB.", ext_no_dot)
    return(file.path(get_AB_dir(), fname))
  }

# @return Directory containg COSMIC mutations counts at 5kb
get_mut_map_5k_dir <- function() {
  return(file.path(
    get_cosmic_trans_script_dir(),
    "..",
    "mut_map",
    "mut_map_5k"
  ))
}

# @return Path to COSMIC mutation counts at 5kb
get_mut_map_5k_path <-
  function(altchr_id = DEF_ALTCHR_ID,
           ext_no_dot = "csv") {
    fname = paste0("mut_", altchr_id, ".", ext_no_dot)
    return(file.path(get_mut_map_5k_dir(), fname))
  }

###################################################################
# Misc utilities
###################################################################

# Load AB CSV data
load_AB <- function(csvpath = get_AB_path()) {
  return(read.csv(
    file = csvpath,
    header = TRUE,
    stringsAsFactors = FALSE
  ))
}

# Load COSMIC mutation counts
load_mut_map <- function(csvpath = get_mut_map_5k_path()) {
  return(read.csv(
    file = csvpath,
    header = TRUE,
    stringsAsFactors = FALSE
  ))
}

###################################################################
# Misc defaults
###################################################################

# Default healthy cell identifier
DEF_HEA_CID = "gm12878"
DEF_HEA_ALTCID = "GM"

# Default cancer cell identifier
DEF_CAN_CID = "K562"
DEF_CAN_ALTCID = "K"

# Default path to cleaned Hi-C data for healthy phenotype
DEF_TRANS_HEA_HIC_PATH = get_hic_clean_ecdf_path(
  cid = DEF_HEA_CID,
  chr_id = alt2stdchr(DEF_ALTCHR_ID),
  ext_no_dot = "rdata"
)

# Default path to AB compartment for healthy phenotype
DEF_TRANS_HEA_AB_PATH = get_AB_path(altcid = DEF_HEA_ALTCID,
                                    altchr_id = DEF_ALTCHR_ID)

# Default path to cleaned Hi-C data for cancer phenotype
DEF_TRANS_CAN_HIC_PATH = get_hic_clean_ecdf_path(
  cid = DEF_CAN_CID,
  chr_id = alt2stdchr(DEF_ALTCHR_ID),
  ext_no_dot = "rdata"
)

# Default path to AB compartment for cancer phenotype
DEF_TRANS_CAN_AB_PATH = get_AB_path(altcid = DEF_CAN_ALTCID,
                                    altchr_id = DEF_ALTCHR_ID)

# Default quantile for labeling 'healthy' locus as specific
DEF_QHEA_SPE = 0.75

# Default quantile for labeling 'cancer' locus as specific
DEF_QCAN_SPE = DEF_QHEA_SPE

# Default quantile for labeling locus as COSMIC enriched
DEF_QCOSM = 0.05

# Default FDR threshold for labeling Hi-C contact as specific
DEF_ALPHA = 0.05

###################################################################
# Transition
###################################################################

# @param contacsHEA - Data.frame of 'healthy' contacts for a single chromosome
# @param contactsCAN - Data.frame of 'cancer' contacts for same chr as contacsHEA
# @param HEA_AB - AB compartments for 'healthy' for same chr
# @param CAN_AB - AB compartments for 'cancer' for same chr
# @param mut_num - Number of cosmic mutations for same chr
# @param QHEA_SPE - Quantile threshold in [0,1] to label a 'healthy' locus as 'specific'
# @param QCAN_SPE - Quantile threshold in [0,1] to label a 'cancer' locus as 'specific'
# @param QCOSM - Quantile threshold in [0,1] to label locus as COSMIC 'mutation enriched'
# @param alpha - FDR for labeling (i, j) contact as 'specific'
# @return list with members
#   $cos - COSMIC enriched transitions counts matrix
#   $cosf - COSMIC enriched transitions frequencies matrix
#   $ncos - non-COSMIC enriched transitions counts matrix
#   $ncosf - non-COSMIC enriched transitions frequencies matrix
get_trans_mats <-
  function(contactsHEA = load_rdata(rdata_path = DEF_TRANS_HEA_HIC_PATH),
           contactsCAN = load_rdata(rdata_path = DEF_TRANS_CAN_HIC_PATH),
           HEA_AB = load_AB(DEF_TRANS_HEA_AB_PATH),
           CAN_AB = load_AB(DEF_TRANS_CAN_AB_PATH),
           mut_num = load_mut_map(),
           QHEA_SPE = DEF_QHEA_SPE,
           QCAN_SPE = DEF_QCAN_SPE,
           QCOSM = DEF_QCOSM,
           alpha = DEF_ALPHA) {
    # Verify assumptions
    stopifnot((QHEA_SPE >= 0.0) && (QHEA_SPE <= 1.0))
    stopifnot((QCAN_SPE >= 0.0) && (QCAN_SPE <= 1.0))
    stopifnot((QCOSM >= 0.0) && (QCOSM <= 1.0))
    stopifnot((alpha >= 0.0) && (alpha <= 1.0))
    
    # Utility joins (i,j) contacts into single locus counts
    get_spe_counts <- function(contacts_df = contactsHEA, alpha) {
      # get the contacts with fdr < alpha
      spe = subset(contacts_df, contacts_df$fdr < alpha)
      # get the contact number for each bin in column i1
      i1_counts = count(spe$i1_abs)
      # change the column's name in order to join data.frames together
      colnames(i1_counts) = c("ID", "i1_num")
      # get the contact number for each bin in column j1
      j1_counts = count(spe$j1_abs)
      # change the column's name in order to join data.frames together
      colnames(j1_counts) = c("ID", "j1_num")
      # join the two data.frames together to get the sum of contacts in i1 and j1
      counts = join(i1_counts, j1_counts, by = "ID", type = "full")
      # change NA to 0
      counts[is.na(counts)] = 0
      counts$num = counts$i1_num + counts$j1_num
      # Delete columns i1 and j1
      counts = counts[, c("ID", "num")]
      return(counts)
    }
    
    hea_spe_counts = get_spe_counts(contacts_df = contactsHEA, alpha = alpha)
    colnames(hea_spe_counts)[which(colnames(hea_spe_counts) == "num")] = "HEAnum"
    can_spe_counts = get_spe_counts(contacts_df = contactsCAN, alpha = alpha)
    colnames(can_spe_counts)[which(colnames(can_spe_counts) == "num")] = "CANnum"
    
    # join the AB data with the contacts data for HEA and CAN
    HEA_AB_all = join(hea_spe_counts, HEA_AB, by = "ID", type = "left")
    CAN_AB_all = join(can_spe_counts, CAN_AB, by = "ID", type = "left")
    HEA_CAN_AB_all = join(HEA_AB_all, CAN_AB_all, by = "ID", type = "full")
    # join the cosmic mutation data
    HEA_CAN_master <-
      join(HEA_CAN_AB_all, mut_num, by = "ID", type = "left")
    # Change the column's name, note 'counts' = cosmic mutation counts
    colnames(HEA_CAN_master) <-
      c("ID", "HEA_freq", "HEA_AB", "CAN_freq", "CAN_AB", "counts")
    # Change NA to 0
    HEA_CAN_master[is.na(HEA_CAN_master)] <- 0
    
    QHEA_SPE_val <-
      unname(quantile(hea_spe_counts$HEAnum, QHEA_SPE))
    QCAN_SPE_val <-
      unname(quantile(can_spe_counts$CANnum, QCAN_SPE))
    QCOSM_val <- unname(quantile(mut_num$counts, QCOSM))
    
    # Select [not]enriched rows
    get_enrich <-
      function(df_master = HEA_CAN_master, colid, qval, enrich) {
        if (enrich) {
          return(df_master[, colid] >= qval)
        }
        return(df_master[, colid] < qval)
      }
    
    # Determine cell type data key
    get_sub_key <-
      function(cell = c("HEA", "CAN")[1],
               ab = c("A", "B")[1],
               spe = c(TRUE, FALSE)[1]) {
        spe_key = if (spe) {
          "spe"
        } else {
          "nspe"
        }
        return(paste0(cell, '_', ab, spe_key))
      }
    
    # Get all data keys for a cell type
    get_cell_keys <- function(cell = c("HEA", "CAN")[1]) {
      return(c(
        get_sub_key(cell = cell, ab = "A", spe = TRUE),
        get_sub_key(cell = cell, ab = "A", spe = FALSE),
        get_sub_key(cell = cell, ab = "B", spe = TRUE),
        get_sub_key(cell = cell, ab = "B", spe = FALSE)
      ))
    }
    
    # COSMIC enrichment key
    get_mut_key <- function(mut = c(TRUE, FALSE)[1]) {
      return(if (mut) {
        "cos"
      } else {
        "ncos"
      })
    }
    
    # Return 0-initialized transition matrix
    init_trans_mat <- function() {
      m = matrix(data = 0.0,
                 nrow = 4,
                 ncol = 4)
      rownames(m) = get_cell_keys(cell = "HEA")
      colnames(m) = get_cell_keys(cell = "CAN")
      return(m)
    }
    
    trans_list = list()
    # COSMIC enriched transitions
    trans_list[[get_mut_key(mut = TRUE)]] = init_trans_mat()
    # Non-COSMIC enriched transitions
    trans_list[[get_mut_key(mut = FALSE)]] = init_trans_mat()
    
    ###################################################
    # Compute transition count tables
    
    HEA_AB_dom = c("A", "B")
    CAN_AB_dom = c("A", "B")
    HEA_SPE_dom = c(TRUE, FALSE)
    CAN_SPE_dom = c(TRUE, FALSE)
    MUT_dom = c(TRUE, FALSE)
    
    for (hea_ab in HEA_AB_dom) {
      hea_ab_sel = HEA_CAN_master$HEA_AB == hea_ab
      for (hea_spe in HEA_SPE_dom) {
        hea_spe_sel = get_enrich(colid = "HEA_freq",
                                 qval = QHEA_SPE_val,
                                 enrich = hea_spe)
        hea_key = get_sub_key(cell = "HEA",
                              ab = hea_ab,
                              spe = hea_spe)
        for (can_ab in CAN_AB_dom) {
          can_ab_sel = HEA_CAN_master$CAN_AB == can_ab
          for (can_spe in CAN_SPE_dom) {
            can_spe_sel = get_enrich(colid = "CAN_freq",
                                     qval = QCAN_SPE_val,
                                     enrich = can_spe)
            can_key = get_sub_key(cell = "CAN",
                                  ab = can_ab,
                                  spe = can_spe)
            for (mut in MUT_dom) {
              mut_sel = get_enrich(colid = "counts",
                                   qval = QCOSM_val,
                                   enrich = mut)
              final_sel = hea_ab_sel &
                can_ab_sel & hea_spe_sel & can_spe_sel & mut_sel
              trans_counts_df = HEA_CAN_master[final_sel,]
              # Update count
              mut_key = get_mut_key(mut = mut)
              stopifnot(0 == trans_list[[mut_key]][hea_key, can_key])
              trans_list[[mut_key]][hea_key, can_key] = nrow(trans_counts_df)
            } # end iteration over MUT_dom
          } # end iteration over CAN_SPE_dom
        } # end iteration over CAN_AB_dom
      } # end iteration over HEA_SPE_dom
    } # end iteration over HEA_AB_dom
    
    # Add normalized matrices
    for (mut in MUT_dom) {
      mut_key = get_mut_key(mut = mut)
      mutf_key = paste0(mut_key, "f")
      mat = trans_list[[mut_key]]
      trans_list[[mutf_key]] = mat / sum(mat)
    }
    q_num=c(QHEA_SPE_val,QCAN_SPE_val,QCOSM_val)
    return(q_num)
  }


get_join_oncogene_master <- function() {
  df_master=get_trans_mats()
  file_start=file.path(get_cosmic_trans_script_dir(), "..", "output", "chr2-start.csv")
  df_start=read.csv(file = file_start,header = TRUE,stringsAsFactors = FALSE)
  file_end=file.path(get_cosmic_trans_script_dir(), "..", "output", "chr2-end.csv")
  df_end=read.csv(file = file_end,header = TRUE,stringsAsFactors = FALSE)
  df_final = join(df_start, df_master, by = "ID", type = "full")
  df_final2 = join(df_end, df_final, by = "ID", type = "full")
  out_dataframe_path = file.path(get_hic_clean_script_dir(),"..","test.Rdata")
  save_rdata(df_final2,out_dataframe_path)
  #return(df_final2)
}



dfmaster=get_trans_mats()
get_join_oncogene_master()

###################################################################
# Launchers
###################################################################

# Directory for storing transition matrices
get_trans_mats_dir <- function() {
  return(file.path(get_cosmic_trans_script_dir(),
                   "..",
                   "output",
                   "trans_mats"))
}

# Path to read/write transition matrices data file
get_trans_mats_path <-
  function(altchr, Qspe, Qcosm, alpha, ext_no_dot = "rdata") {
    fname = paste0(altchr, "_", Qspe, "_", Qcosm, "_", alpha, ".", ext_no_dot)
    return(file.path(get_trans_mats_dir(), fname))
  }

# Transition matrices launcher: default chromosomes (alt format)
DEF_LAUNCH_TRANS_ALTCHRS = c(
  "1",
  "2",
  "3",
  "4",
  "5",
  "6",
  "7",
  "8",
  "10",
  "11",
  "12",
  "13",
  "14",
  "15",
  "16",
  "17",
  "18",
  "19",
  "20",
  "21",
  "22",
  "X"
)

# Transition matrices launcher: default locus Hi-C specific contact count quantiles
DEF_LAUNCH_TRANS_QSPES = c(0.5, 0.75, 0.9, 0.95)

# Transition matrices launcher: default COSMIC mutation count quantiles
DEF_LAUNCH_TRANS_QCOSMS = c(0.01, 0.05, 0.1, 0.25, 0.5, 0.75, 0.9)


# Transition matrices launcher: default alpha (FDR) thresholds for calling
# specific Hi-C contact tuples
DEF_LAUNCH_TRANS_ALPHAS = c(0.01, 0.05)

# Launcher to generate transition matrices for various parameter combinatons
launch_trans_mats <-
  function(altchrs = DEF_LAUNCH_TRANS_ALTCHRS,
           Qspes = DEF_LAUNCH_TRANS_QSPES,
           Qcosms = DEF_LAUNCH_TRANS_QCOSMS,
           alphas = DEF_LAUNCH_TRANS_ALPHAS,
           hea_cid = DEF_HEA_CID,
           hea_altcid = DEF_HEA_ALTCID,
           can_cid = DEF_CAN_CID,
           can_altcid = DEF_CAN_ALTCID,
           overwrite = DEF_SHOULD_OVERWRITE) {
    
    for (altchr in altchrs) {
      contactsHEA = load_rdata(rdata_path = get_hic_clean_ecdf_path(
        cid = hea_cid,
        chr_id = alt2stdchr(altchr),
        ext_no_dot = "rdata"
      ))
      contactsCAN = load_rdata(rdata_path = get_hic_clean_ecdf_path(
        cid = can_cid,
        chr_id = alt2stdchr(altchr),
        ext_no_dot = "rdata"
      ))
      HEA_AB = load_AB(get_AB_path(altcid = hea_altcid,
                                   altchr_id = altchr))
      CAN_AB = load_AB(get_AB_path(altcid = can_altcid,
                                   altchr_id = altchr))
      mut_map = load_mut_map(get_mut_map_5k_path(altchr_id = altchr,
                                                 ext_no_dot = "csv"))
      for (alpha in alphas) {
        for (Qspe in Qspes) {
          for (Qcosm in Qcosms) {
            out_matrices_path = get_trans_mats_path(
              altchr = altchr,
              Qspe = Qspe,
              Qcosm = Qcosm,
              alpha = alpha,
              ext_no_dot = "rdata"
            )
            if (!overwrite && file.exists(out_matrices_path)) {
              print(paste0("Skipping cached data: ", out_matrices_path))
              next
            }
            out = get_trans_mats(
              contactsHEA = contactsHEA,
              contactsCAN = contactsCAN,
              HEA_AB = HEA_AB,
              CAN_AB = CAN_AB,
              mut_num = mut_map,
              QHEA_SPE = Qspe,
              QCAN_SPE = Qspe,
              QCOSM = Qcosm,
              alpha = alpha
            )
            save_rdata(data = out, rdata_path = out_matrices_path)
          } # End iteration over Qcosms
        } # End iteration over Qspes
      } # End iteration over alphas
    } # End iteration over chromosomes
  } # End function
