gen_data=dlmread('spe_K_chr22.csv',',',1,0);
matrix_data = zeros(30);
for i=1:30
    mn=0;   
    for j=1:226
        c=((i-1)*226)+j;
        mn = mn + gen_data(c,3);
    end
    mn= mn/226;
    matrix_data(gen_data(i,1),gen_data(i,2))=mn;%gen_data(i*231,3);
end   
A = randi([10,60],100,100);
colormap('autumn')
%heatmap(matrix_data)
imagesc(matrix_data)
colorbar