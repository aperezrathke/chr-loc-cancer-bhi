###################################################################
# computer the pvalue for trans__mats in chisquare test and z test
###################################################################

library(stats)

###################################################################
###################################################################
# Path to this script
###################################################################

# Cached directory to script
# https://stackoverflow.com/questions/3452086/getting-path-of-an-r-script
COSMIC_PVALS_SCRIPT_DIR = getSrcDirectory(function(x) {
  x
})

# getSrcDirectory trick will not work via Rscript. If this is the
# case, then COSMIC_PVALS_SCRIPT_DIR will be length zero.
# http://stackoverflow.com/questions/1815606/rscript-determine-path-of-the-executing-script
if (length(COSMIC_PVALS_SCRIPT_DIR) == 0) {
  COSMIC_PVALS_SCRIPT_DIR = dirname(scriptName::current_filename())
}

# @return Path to directory containing this script
get_cosmic_pvals_script_dir <- function() {
  return(COSMIC_PVALS_SCRIPT_DIR)
}

###################################################################
# Source utility script(s)
###################################################################

if (!exists("SOURCED_cosmic_trans")) {
  source(file.path(get_cosmic_pvals_script_dir(), "cosmic_trans.R"))
}

###################################################################
# pvalue
###################################################################


get_trans_mats_pvalue_chisq <- function() {
  pvalue = c() 
  mat_files_name = c()
  mats_files = list.files(path = get_trans_mats_dir())
  for (mat_file in mats_files) {
    mat_files_name = c(mat_files_name, mat_file)
  }
  for (mat_file in mats_files) {
    mat_f = load_rdata(rdata_path = file.path(get_trans_mats_dir(), mat_file))
    for (row_index in c(1:nrow(mat_f$cos))) {
      for (col_index in c(1:ncol(mat_f$cos))) {
        cos_test = mat_f$cos[row_index, col_index]
        cos_other = sum(mat_f$cos) - cos_test
        ncos_test = mat_f$cos[row_index, col_index]
        ncos_other = sum(mat_f$ncos) - ncos_test
        chisq_test_mat = as.table(rbind(c(cos_test, ncos_test),
                                        c(cos_other, ncos_other)))
        chisq = chisq.test(chisq_test_mat)
        pvalue = c(pvalue, chisq$p.value)
      }
    }
  }
  p_adjust = p.adjust(pvalue, "BY")
  pvalue_adjust = matrix(
    p_adjust,
    nrow = length(mats_files),
    ncol = (nrow(mat_f$cos) * ncol(mat_f$cos)),
    byrow = TRUE
  )
  pvalue_adjust = as.data.frame(pvalue_adjust)
  pvalue_adjust$file_name = mat_files_name
  colnames(pvalue_adjust) = c(
    "type1",
    "type2",
    "type3",
    "type4",
    "type5",
    "type6",
    "type7",
    "type8",
    "type9",
    "type10",
    "type11",
    "type12",
    "type13",
    "type14",
    "type15",
    "type16",
    "mat_name"
  )
  out_trans_mats_pvalue_path = file.path(get_hic_clean_script_dir(),
                                         "..",
                                         "output",
                                         "pvalue_chisq_all(BY).csv")
  save_csv(x = pvalue_adjust , csv_path = out_trans_mats_pvalue_path)
  
}
