# -*- coding: utf-8 -*-
"""
Created on Sat Jan 12 14:07:34 2019

@author: samir
"""

import numpy
import csv
from operator import itemgetter
#sortToFile = open("c:\\ss.txt", "w")
#reader = csv.reader(open("c:\\chr3_5kbRAWobserved.txt"), delimiter="\t")
sortToFile = open("c:\\ss-K562.txt", "w")
reader = csv.reader(open("c:\\chr3.txt"), delimiter="\t")
counter=0
for line in sorted(reader, key=itemgetter(2), reverse=True):
    #print(line)
    counter +=1
    sortToFile.write("%s\n" % line)
    if (counter>80000) :
        break
    