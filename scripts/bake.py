#!/usr/bin/python 

 # Bake on machine without AVX2 suport
 
import os

###################################################
# Script paths
###################################################

# Path to directory containing this script
# https://stackoverflow.com/questions/4934806/how-can-i-find-scripts-directory-with-python
SCRIPT_DIR = os.path.abspath(os.path.dirname(os.path.realpath(__file__)))

# Path to .bun file
BUN_PATH=os.path.join(SCRIPT_DIR, "config","cb.bun")

# Output directory
OUT_DIR = os.path.join(SCRIPT_DIR, "..", "output")

# Compilation target
COMPILE="linux-gnu-threaded linux-gnu-cmdlets"

###################################################
# Bake
###################################################

BAKE_SCRIPT_PATH=os.path.join(SCRIPT_DIR,"..","..","..","folder","scripts","Template", "bake.py")

str_cmd = "python "+ BAKE_SCRIPT_PATH +" --bun "+BUN_PATH+" --out "+OUT_DIR+" --compile "+ COMPILE +" --force"

print "Running: " + str_cmd
result = os.popen(str_cmd).read()
print result

