#!/bin/bash

# Path to this script's directory
SCRIPT_PATH="$(readlink -f $0)"
SCRIPT_DIR="$(dirname $SCRIPT_PATH)"
SCRIPT_DIR=$(cd "$SCRIPT_DIR"; pwd)

# Build command line
cmd="Rscript --vanilla $SCRIPT_DIR/launch_hic_clean_ecdf.R"

# Run command line
echo "Running:"
echo $cmd
$cmd
echo "Finished."
