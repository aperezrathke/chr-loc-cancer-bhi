#!/bin/bash

# Directory containing this script
SCRIPT_DIR="$(dirname "$(readlink -f "$0")")"

# Base output directory
OUT_DIR="$SCRIPT_DIR/../output"

# Path to juicer tools
JUICER_PATH="$OUT_DIR/build/tools/juicer_tools.jar"

# Cell identifiers
CELL_ID=("gm12878" "K562")

# Cloud URLs for Hi-C data
# https://www.aidenlab.org/data.html
HIC_URL=("https://hicfiles.s3.amazonaws.com/hiseq/gm12878/in-situ/combined.hic" "https://hicfiles.s3.amazonaws.com/hiseq/k562/in-situ/combined.hic")

# Prefix to directory for storing Hi-C
HIC_RAW_DIR_PREFIX="$OUT_DIR/hic.raw"

# Chromosome identifiers
CHR_ID=("1" "2" "3" "4" "5" "6" "7" "8" "9" "10" "11" "12" "13" "14" "15" "16" "17" "18" "19" "20" "21" "22" "X")

# Iterate over cell identifiers
for ix_cell_id in "${!CELL_ID[@]}"
do
    # Cell identifier
    cell_id="${CELL_ID[$ix_cell_id]}"
    # Hi-C url
    hic_url="${HIC_URL[$ix_cell_id]}"
    # Target raw Hi-C data directory
    hic_dir="$HIC_RAW_DIR_PREFIX.$cell_id"
    mkdir -p "$hic_dir"
    # Iterate over chromosomes
    for chr_id in "${CHR_ID[@]}"
    do
        # Pull raw Hi-C
        hic_file="chr$chr_id.txt"
        hic_path="$hic_dir/$hic_file"
        cmd="java -jar $JUICER_PATH dump observed KR $hic_url $chr_id $chr_id BP 5000 $hic_path"
        echo "Running: $cmd"
        $cmd
        # Compress Hi-C
        echo "Compressing $hic_file"
        pushd .
        cd "$hic_dir"
        gzip "$hic_file"
        popd
    done # End iteration over chromosomes
done # End iteration over cell identifiers

echo "Finished"
