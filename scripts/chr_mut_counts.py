



##analysis cosmic mutation data for every chromosome，and get the mutation counts for every bin
import re
import csv

with open('chr22_mut.csv','r') as csvfile:
	reader=csv.reader(csvfile)
	column=[column[2]for column in reader]
	b1=[]
	b2=[]
	b3=[]
	b4=[]
	b=[b1,b2,b3,b4]

	for i in range(len(column)):
		a=column[i]
		a=a[a.find(':'):a.find('-')]
		a=int(a[1:])
		b1.append(a/5000+1)
		b2.append(a/10000+1)
		b3.append(a/50000+1)
		b4.append(a/100000+1)

	# print b1
	for unit in range(4):
		temp=set(b[unit])
		counter=[]
		number=[]
		for each_temp in temp:
			count=0
			for each_b in b[unit]:
				if each_temp==each_b:
					count+=1
			counter.append(count)
			number.append(each_temp)
			# print counter

		csvfile=open(str(unit)+'_22.csv','w')
		writer=csv.writer(csvfile,delimiter=',')
		header=['ID','counts']
		writer.writerow(header)
		writer.writerows(zip(number,counter))
		csvfile.close()
