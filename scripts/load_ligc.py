#!/usr/bin/python
# -*- coding: utf-8 -*-

# Example script for loading in a gzip-compressed ligc file in CSV format

###########################################
# Imports
###########################################

import gzip

import numpy as np

from collections import namedtuple

###########################################
# Named tuple
##########################################

# Ligc data
#   .i0 - numpy array i of (i,j) chromatin contacts (0-based indexing)
#   .j0 - numpy array j of (i,j) chromatin contacts (0-based indexing)
#   .logw - log weight (improper) of ligc sample
Ligc = namedtuple("Ligc", "i0, j0, logw")

##########################################
# Globals
###########################################

# Prefix string ligc file denoting log weight
LOGW_PREFIX = "# LOG WEIGHT: "

###########################################
# File I/O
###########################################

# @return Ligc named tuple
def load_ligc_csv_gz(fpath):
    '''Load gzip compressed ligc csv file'''
    i0v = []
    j0v = []
    logw = 0.0
    with gzip.open(fpath, 'rb') as fligc:
        strligc = fligc.read()
        lines = strligc.splitlines()
        for line in lines:
            # Remove leading and trailing whitespace
            line = line.strip()
            # Ignore empty (all whitespace) lines
            if not line:
                continue
            # Check if line contains log weight
            if line.startswith(LOGW_PREFIX):
                # Extract log weight
                logw = float(line[len(LOGW_PREFIX):])
                continue
            # Ignore comment lines
            if line.startswith('#'):
                continue
            # Process ligation tuple
            tup = line.split(',')
            assert len(tup) == 2
            i0 = int(tup[0])
            j0 = int(tup[1])
            assert i0 <= j0
            i0v.append(i0)
            j0v.append(j0)
    assert len(i0v) == len(j0v)
    ligc = Ligc(i0=np.array(i0v), j0=np.array(j0v), logw=logw)
    return ligc
