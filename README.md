Repository for [IEEE-EMBS International Conference on Biomedical and Health Informatics (BHI) 2019](https://www.bhi-bsn-2019.org/) conference paper:

*Alterations in Chromatin Folding Patterns in Cancer Variant-Enriched Loci*

Please contact *jliangATuicDOTedu* for access to 3-D chromatin folding software
